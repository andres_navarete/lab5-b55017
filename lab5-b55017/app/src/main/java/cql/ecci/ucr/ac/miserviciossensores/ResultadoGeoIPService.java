package cql.ecci.ucr.ac.miserviciossensores;

public class ResultadoGeoIPService {
    private String NombrePais;
    private String CodigoPais;

    public String getNombrePais() {
        return NombrePais;
    }

    public void setNombrePais(String nombrePais) {
        NombrePais = nombrePais;
    }

    public String getCodigoPais() {
        return CodigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        CodigoPais = codigoPais;
    }

    @Override
    public String toString() {
        return "GeoIPService [CountryName=" + NombrePais + ", CountryCode=" + CodigoPais + "]";
    }
}
